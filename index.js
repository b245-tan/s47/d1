// console.log("JS DOM - Manipulation")

// [SECTION] Document Object Model (DOM)
/*
	- Allows us to access or modify the properties of an html element in a webpage
	- It is a standard on how to get, change, add, or delete HTML elements
	-We will be focusing only with DOM in terms of managing forms

*/

	// For selecting HTML elements, we will be using document.querySelector / getElementById
	/*
		Syntax:
			document.querySelector("html element")
			document.querySelectorAll("html element")
	*/
	
	// CSS Selectors
	/*
		1. Class selector (.)
		2. ID selector (#)
		3. Tag selector (html tags)
		4. Universal selector (*)
		5. Attribute ([attribute])
	*/
	
	// querySelectorAll
	let universalSelector = document.querySelectorAll("*");

	// querrySelector
	let singleUniversalSelector = document.querySelector("*");

	console.log(universalSelector)
	console.log(singleUniversalSelector)

	// AllClass
	let classSelector = document.querySelectorAll(".full-name")
	console.log(classSelector)

	// SingleClass
	let singleClassSelector = document.querySelector(".full-name")
	console.log(singleClassSelector)

	// Id
	let IdSelector = document.querySelector("#txt-first-name")
	console.log(IdSelector)

	// Tag
	let tagSelector = document.querySelectorAll("input")
	console.log(tagSelector)

	// Specific span
	let spanSelector = document.querySelector("span[id]")
	console.log(spanSelector)


	// getElement
		let element = document.getElementById("fullName")
		console.log(element)

		/*element = document.getElementsByClassName("fullName")
		console.log(element)
		*/

// [SECTION] Event Listeners
/*
	- whenever a user interacts with a webpage, this action is considered as an event
	- working with events is a large part of creating interactivity in a web page
	- specific function that will be triggered if the event happened.
*/
	// The function used is "addEventListener" which takes two arguments
		// the first argument is a string identifying the event
		// the second argument is a function that the listener will trigger once the "specified event" occurs.

		let txtFirstName = document.querySelector("#txt-first-name");

		// Add event listener
		txtFirstName.addEventListener("keyup", ()=> {
			console.log(txtFirstName.value);

			spanSelector.innerHTML = `${txtFirstName.value} ${txtLastName.value}`

		})


		let txtLastName = document.querySelector("#txt-last-name");

		txtLastName.addEventListener("keyup", ()=> {
			spanSelector.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
		})

		// note : value refers to content of a form; innerHTML refers to content between HTML tags


// [ACTIVITY]------------------------------------------------

		const selectColor = document.querySelector('#text-color')

		// Default color black
		let colorChoose = document.querySelector('option[value=""')

		colorChoose.style.color = "default";



		// Red
		let colorRed = document.querySelector('option[value="red"')

		colorRed.style.color = "red";


		// Green
		let colorGreen = document.querySelector('option[value="green"')

		colorGreen.style.color = "green";



		// Blue
		let colorBlue = document.querySelector('option[value="blue"')

		colorBlue.style.color = "blue";


		// Change text Color
		selectColor.addEventListener("change", (event)=> {
			const result = document.querySelector('.result')
			spanSelector.style.color = event.target.value
			
		})